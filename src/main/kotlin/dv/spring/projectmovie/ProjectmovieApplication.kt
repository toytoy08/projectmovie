package dv.spring.projectmovie

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ProjectmovieApplication

fun main(args: Array<String>) {
    runApplication<ProjectmovieApplication>(*args)
}
